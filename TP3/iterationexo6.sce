function [relres]=Jacobi(A,b,max,x0)
nbr=0 ;
 funcprot(0);
n=size(A,1);




resvec=zeros(max,1);
 
 
D = eye(A).*A

E= -(tril(A) -D)
F=-(triu(A)- D )

res = b-A*x0
relres=norm(res)/norm(b);
 invD=inv(D)
 while ( (relres>0.00001 )&(nbr<max) )

     
     nbr = nbr+1
    x1=invD*(E+F)*x0+ ( invD*b )
    //on garde le x predecent 

    x0=x1
    res = b-A*x1
 //  relres=norm(res)/norm(b)
//   relres=norm(res)
    relres(nbr)=norm(res)
       
        
 end 
 
 
 


 
  
 endfunction

function [resvec]=GaussSeidel(A,b,max,x0)
    
nbr=0 ;
funcprot(0);

n=size(A,1);
x1=ones(n,1);


resvec=zeros(max,1);
 
 
D = eye(A).*A

E= -(tril(A) -D)
F=-(triu(A)- D )
res=b-A*x0
invDE = inv(D-E);
relres=norm(res)/norm(b);

 while ( (relres>0.000001 )&(nbr<max) )

     
     nbr = nbr+1
    x1= (invDE) *F * x0 +(invDE) *b


    res=b-A*x1
    relre=norm(res)
          x0=x1
    resvec(nbr)=relre
      
        
   end 
   
 







       // relres(i) = norm(r0);
        
    
  


endfunction 

max=3;


A=[2 -1 0 
-1 2 1;
0 -1 2 ];
 
b = [ -5 ; 0 ; 5 ];
x0=zeros(3,1)
f=Jacobi(A,b,max,x0)
disp(f)
g=GaussSeidel(A,b,max,x0)
disp (g)
plot(1:3,g)
plot(1:3,f)


